#!/bin/bash

# TODO; parametrize user name here when mounting the setup-dir volume.
# Allow X server connection
# https://www.mit.edu/~arosinol/2019/08/06/Docker_Display_GUI_with_X_server/
xhost +local:root
docker run -it --rm \
       --env="DISPLAY" \
       --env="QT_X11_NO_MITSHM=1" \
       --volume="/tmp/.X11-unix:/tmp/.X11-unix:rw" \
       aj-machine
# Disallow X server connection
xhost -local:root

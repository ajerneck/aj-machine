FROM ubuntu:focal

RUN apt-get update

# install tzdata
# https://stackoverflow.com/a/44333806/1072349
ENV DEBIAN_FRONTEND=noninteractive
RUN ln -fs /usr/share/zoneinfo/America/New_York /etc/localtime
RUN apt-get install -y tzdata
RUN dpkg-reconfigure --frontend noninteractive tzdata

# install basic X
RUN apt-get install -y --no-install-recommends xorg openbox


# install software
RUN apt-get -y install git \
  emacs

## create the user
RUN useradd -ms /bin/bash alexander
USER alexander
RUN git clone https://github.com/syl20bnr/spacemacs ~/.emacs.d

## add dotfiles from source directory.
ADD ./ /home/alexander/setup/

## make symlinks.
WORKDIR /home/alexander/setup

RUN ./copy-dotfiles.sh

